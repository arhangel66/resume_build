from django.contrib import admin
from django.contrib.admin import register, ModelAdmin
from django.utils.html import format_html

from .models import Resume, AdditionalSkills, WorkItem, EducationItem


class AdditionalSkillInline(admin.TabularInline):
    model = AdditionalSkills
    fields = ['skill']


class WorkItemInline(admin.TabularInline):
    model = WorkItem
    fields = ['position', 'start_date', 'end_date', 'employer']


class EducationItemInline(admin.TabularInline):
    model = EducationItem
    fields = ['school_type', 'school_name', 'city', 'graduation_date']


@register(Resume)
class ResumeAdmin(ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'created', 'preview')
    inlines = [EducationItemInline, WorkItemInline, AdditionalSkillInline]

    def preview(self, obj):
        html = """
        <a href="/resume/action/?resume={resume_id}&action=preview">preview</a>
        <a href="/resume/action/?resume={resume_id}&action=pdf">pdf</a>
        """.format(resume_id=obj.id)
        return format_html(html)
