import datetime

from django.db import models
from model_utils.models import TimeStampedModel

SHOOL_CHOICES = (
    ('High School', 'high'),
    ('Collegue', 'collegue'),
    ('Other', 'other'),
)


class Resume(TimeStampedModel):
    first_name = models.CharField(max_length=500, default='John')
    last_name = models.CharField(max_length=500, default='Doe')
    country = models.CharField(max_length=500, blank=True, null=True)
    zip_code = models.CharField(max_length=500, blank=True, null=True)
    city = models.CharField(max_length=500, blank=True, null=True)
    state = models.CharField(max_length=500, blank=True, null=True)
    address_line_1 = models.CharField(max_length=500, blank=True, null=True)
    address_line_2 = models.CharField(max_length=500, blank=True, null=True)
    phone = models.CharField(max_length=500, blank=True, null=True)
    email = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return "%s %s resume, %s" % (self.first_name, self.last_name, self.created)



class ItemMixin(TimeStampedModel):
    resume = models.ForeignKey(Resume, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class WorkItem(ItemMixin):
    position = models.CharField(max_length=200)
    start_date = models.DateField(max_length=200)
    end_date = models.DateField(max_length=200, default=datetime.date.today)
    employer = models.CharField(max_length=200)


class EducationItem(ItemMixin):
    school_type = models.CharField(max_length=500, choices=SHOOL_CHOICES)
    school_name = models.CharField(max_length=500)
    city = models.CharField(max_length=500)
    graduation_date = models.DateField()


class AdditionalSkills(ItemMixin):
    skill = models.CharField(max_length=200)
