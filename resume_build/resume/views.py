from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views import View

from resume_build.resume.models import Resume
from resume_build.resume.pdf_helper import generate_pdf


class ResumeAction(View):
    def get(self, request, resume_id, action):
        o_resume = get_object_or_404(Resume, pk=resume_id)
        print(o_resume)
        resp = HttpResponse(content_type='application/pdf')
        # resp['Content-Disposition'] = 'attachment; filename=' + 'resume.pdf'
        result = generate_pdf('pdf/classic_bnw_template.html', response=resp, context={'r': o_resume})
        # print(resume_id, action)
        return result
