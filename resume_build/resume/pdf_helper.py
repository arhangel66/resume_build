# -*- coding: utf-8 -*-
from logging import getLogger

from weasyprint import CSS, HTML
from weasyprint.formatting_structure.boxes import TableBox

from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _


logger = getLogger(__name__)


def generate_pdf(template, file_object=None, response=None, context=None, minify=False):
    """
    Generate PDF content from given template and context.
    - If the file_object is provided, generated content will be write to that
      file then return it.
    - If the response is provided, it will be returned with generated content
      inside.
    - Otherwise, the PDF as bytestring will be returned.
    """
    style = CSS(string='@page {font-family: Arial, Verdana, Helvetica, sans-serif;}')

    try:
        # write_obj() return None if target is an file object. Then we return
        # the file_object itself.

        full_html = render_to_string(template, context)
        pdf_doc = HTML(string=full_html).render(stylesheets=[style])

        pdf_file = pdf_doc.write_pdf(target=file_object)
    except:
        logger.exception(
            _('Error when exporting PDF content using template {}.').format(
                template)
        )

    if file_object is not None:
        return file_object

    # Add pdf content into response object if provided
    if response is not None:
        response.content = pdf_file
        return response

    return pdf_file
